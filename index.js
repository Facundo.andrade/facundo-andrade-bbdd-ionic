const express = require("express"); // es el nostre servidor web
const cors = require('cors'); // ens habilita el cors recordes el bicing???
const bodyParser = require('body-parser'); // per a poder rebre jsons en el body de la resposta
const app = express();
const baseUrl = '/miapi';
app.use(cors());

app.use(bodyParser.urlencoded({ extended: false })); //per a poder rebre json en el reuest
app.use(bodyParser.json());

//La configuració de la meva bbdd
ddbbConfig = {
   user: 'facundo-andrade-7e3',
   host: 'postgresql-facundo-andrade-7e3.alwaysdata.net',
   database: 'facundo-andrade-7e3_ionic',
   password: 'Lelele123',
   port: 5432
};
//El pool es un congunt de conexions
const Pool = require('pg').Pool
const pool = new Pool(ddbbConfig);

//Exemple endPoint
//Quan accedint a http://localhost:3000/miapi/test   ens saludará
const getMotos = (request, response) => {

   const marca=request.query.marca || ''
    let consulta=''
    if(marca){
        consulta = `SELECT * FROM  motos where marca = '${marca}'`
    }else{
        consulta = "SELECT * FROM  motos"
    }

   pool.query(consulta, (error, results) => {
       if (error) {
           throw error
       }
       //a qui retornem la el status 200 (OK) i en el cos de la resposta les motos en json
       response.status(200).json(results.rows)
       //console.log(results.rows);
   });
}

app.get(baseUrl + '/getmotos', getMotos);

//aqui un exemple de post que pasarem per body
const subirMoto = (request, response) => {

   console.log(request);

   const body = request.body;

   let consulta = "INSERT into motos values(default,$1,$2,$3,$4,$5)";

   pool.query(consulta, [body.marca,body.modelo, body.year, body.foto,body.precio], (error, results) => {
      if (error) {
          throw error
      }
      //a qui retornem la el status 200 (OK) i en el cos de la resposta les motos en json
      //response.status(200).json(results.rows)
      //console.log(results.rows);
      response.send('Hola aixo es el que m\'arriva' + JSON.stringify(request.body))
  });

   

}

app.post(baseUrl + '/postmotos', subirMoto);

const deleteMoto = (request, response) => {
   const motoId = request.params.id;

   console.log(request.params);
 
   var consulta = `DELETE FROM motos WHERE id=${motoId};`;
   pool.query(consulta, (error, results) => {
     if (error) {
       throw error;
     }
     if (results.rowCount != 0) {
       response.status(200).json(results.rows);
     } else {
       response.status(200).json({ response: "No s'ha trobat aquesta moto" });
     }
   });
 };

 app.delete(baseUrl + "/getmotos/:id", deleteMoto);



//Inicialitzem el servei
const PORT = process.env.PORT || 3000; // Port
const IP = process.env.IP || null; // IP

app.listen(PORT, IP, () => {
   console.log("El servidor está inicialitzat en el puerto " + PORT);
});


